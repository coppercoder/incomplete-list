$(document).ready(function() {
  $('#first.line').css('margin-top', $(window).height() + 'px');
  $('.line').css('margin-bottom', $(window).height() + 'px');
});

$(window).scroll(function(event) {
  let scrollTop = $(window).scrollTop();
  $('.line').each(function() {
    let y = scrollTop - $(this).offset().top + $(window).height();
    let height = $(this).height() - 4;
    if (y + height < 0 || y > $(window).height() + height) return;
    
    $('#background').css('background-image', 'url("' + $(this).attr('data-img') + '")');
    
    let fracScrolled = y / ($(window).height() + height);
    console.log(fracScrolled);
    
    let filter;
    if (fracScrolled < 0.2) {
      // first 20%: fade in
      filter = 'opacity(' + Math.max(fracScrolled / 0.2, 0) + ')';
    } else if (fracScrolled > 0.3 && fracScrolled < 0.7) {
      // 40% to 70%: change to grayscale
      filter = 'grayscale(' + (((fracScrolled - 0.3) / 0.4) * 100) + '%)';
    } else if (fracScrolled >= 0.8) {
      // 80% to 100%: fade out
      filter = 'grayscale(100%) opacity(' + (1 - ((fracScrolled - 0.8) / 0.2)) + ')';
    }
    $('#background').css({'-webkit-filter': filter, 'filter': filter});
  });
});

var images = new Array();
$('.line').each(function() {
  let i = images.length;
  images[i] = new Image();
  images[i].src = $(this).attr('data-img');
});